run ../system_model;
addpath('../utils');

% Wait a few seconds before starting the input sequence
sec_before_start = 5;

% Discretize model
h = 0.25;
[A, B] = c2d(Ac, Bc, h);

% Optimization parameters
q1 = 1;
q2 = 1;
alpha = 0.2;
beta = 20;
lambda_t = 2*pi/3; % 'obstacle' position
lambda_f = 0; % final position

% Problem size
n_x = size(A,2);
n_u = size(B,2);

% Initial values
lambda_0 = pi;
r_0 = 0;
p_0 = 0;
p_dot_0 = 0;
e_0 = 0;
e_dot_0 = 0;
x0 = [lambda_0 r_0 p_0 p_dot_0 e_0 e_dot_0]';
u0 = zeros(n_u, 1);

% Time horizon and initialization
N  = 45;			% Time horizon for states
M  = N;				% Time horizon for inputs
n_z = N*n_x+M*n_u;
z  = zeros(n_z,1);	% Initialize z for the whole horizon
if exist('z_old')
    z0 = z_old;     % Initial value for optimization
else
    z0 = z;
end
z0(1) = lambda_0;

% Bounds
ul = -30*pi/180*ones(n_u,1); % Lower bound on pitch/elev ref
uu = 30*pi/180*ones(n_u,1); % Upper bound on pitch/elev ref
xl = -Inf*ones(n_x,1);	% Lower bound on states (no bound)
xu = Inf*ones(n_x,1);	% Upper bound on states (no bound)

% Generate objective function parameters
Q1 = zeros(n_x,n_x);
Q1(1,1) = 1; % Weight on travel
P1 = zeros(n_u, n_u);
P1(1,1) = q1;
P1(2,2) = q2;
Q = 2*genq2(Q1,P1,N,M,n_u);
c = [kron(ones(N,1),[-2*lambda_f;0;0;0;0;0]); 
    zeros(M*n_u,1)];

% Calculate feedback gain
R_lqr = P1;
Q_lqr = diag([1 0.05 0.05 0.05 1 0.05]);
K_lqr = dlqr(A,B,Q_lqr,R_lqr);

% Generate linear constraints from the model
Aeq = [[zeros(n_x,n_x*N); 
		kron(eye(N-1),-A), zeros(n_x*(N-1),n_x)] ...
		+ eye(n_x*N), kron(eye(N),-B)];
beq = [A*x0; zeros(n_x*(N-1),1)];

lb = [kron(ones(N,1),xl);kron(ones(N,1),ul)];
ub = [kron(ones(N,1),xu);kron(ones(N,1),uu)];
delta_s = 3*pi/180;
lb((N-1)*n_x:N*n_x-1) = -delta_s*ones(n_x,1); %We want the last states to be small
ub((N-1)*n_x:N*n_x-1) = delta_s*ones(n_x,1); %We want the last states to be small
lb(end-(n_u-1):end) = zeros(n_u,1); %We want the last input to be zero
ub(end-(n_u-1):end) = zeros(n_u,1); %We want the last input to be zero

% Make cost function and nonlinear constraint
f = @(z) z'*Q*z + c'*z;
nonl_constr = @(z) (e_constraint(z, N, n_x, x0, lambda_t, alpha, beta));

% Solve Nonlinear problem
tic
z = fmincon(f, z0, [], [], Aeq, beq, lb, ub, nonl_constr);
optimTime = toc;
z_old = z;

% % Calculate objective value TODO why?
% phi1 = 0.0;
% PhiOut = zeros(N*n_x+M*n_u,1);
% for i=1:N*n_x+M*n_u
%   phi1=phi1+Q(i,i)*z(i)*z(i);
%   PhiOut(i) = phi1;
% end


% Extract control inputs and states
%u  = [z(N*n_x+1:N*n_x+M*n_u);z(N*n_x+M*n_u)];
u  = [z(N*n_x+1:end)];
u1 = [u(1:n_u:M*n_u);0];
u2 = [u(2:n_u:M*n_u);0];
x1 = [x0(1);z(1:n_x:N*n_x)];
x2 = [x0(2);z(2:n_x:N*n_x)];
x3 = [x0(3);z(3:n_x:N*n_x)];
x4 = [x0(4);z(4:n_x:N*n_x)];
x5 = [x0(5);z(5:n_x:N*n_x)];
x6 = [x0(6);z(6:n_x:N*n_x)];

num_zeros = sec_before_start/h;
Zeros = zeros(num_zeros,1);
Ones  = ones(num_zeros,1);
u1 = [Zeros; u1; Zeros];
u2 = [Zeros; u2; Zeros];
x1 = [pi*Ones; x1; Zeros];
x2 = [Zeros; x2; Zeros];
x3 = [Zeros; x3; Zeros];
x4 = [Zeros; x4; Zeros];
x5 = [Zeros; x5; Zeros];
x6 = [Zeros; x6; Zeros];

% figure
t = [0:h:h*(length(u1)-1)]';
lambda = x1;
lambda_dot = x2;
pitch = x3;
pitch_dot = x4;
elev = x5;
edot = x6;
pitch_ref = u1;
elev_ref = u2;

figure(2)
subplot(811)
stairs(t,u1),grid
ylabel('p_{ref}')
subplot(812)
stairs(t,u2),grid
ylabel('e_{ref}')
subplot(813)
plot(t,x1,'m',t,x1,'mo'),grid
ylabel('lambda')
subplot(814)
plot(t,x2,'m',t,x2','mo'),grid
ylabel('r')
subplot(815)
plot(t,x3,'m',t,x3,'mo'),grid
ylabel('p')
subplot(816)
plot(t,x4,'m',t,x4','mo'),grid
xlabel('tid (s)'),ylabel('pdot')
subplot(817)
plot(t,x5,'m',t,x5','mo'),grid
xlabel('tid (s)'),ylabel('elev')
subplot(818)
plot(t,x6,'m',t,x6','mo'),grid
xlabel('tid (s)'),ylabel('edot')

save('../../report/figs/mat_files/fmincon_p44','t','pitch_ref', 'elev_ref', 'lambda','lambda_dot', 'pitch','pitch_dot', 'elev', 'edot');
