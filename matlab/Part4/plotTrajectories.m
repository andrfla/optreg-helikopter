subplot(211);hold on % elevation
stairs(180/pi*x5,'r')
e_con = 180./pi.*alpha*exp(-beta*((x1-lambda_t)).^2);
stairs(e_con)

legend('Calculated elevation', 'Elevation constraint');

subplot(212); hold on; % travel
stairs(x1,'b');
legend('Measured travel','Calculated travel');