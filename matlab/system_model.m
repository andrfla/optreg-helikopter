run initF
addpath('utils/')
% Linear system dynamics in the form
% x' = Ax + Bu

% Travel and pitch dynamics
Ac_lp = [	0		1		0			0		;
            0		0		-K_2		0       ;
            0		0		0			1		;
            0		0		-K_1*K_pp	-K_1*K_pd];

Bc_lp = [	0	 ;
            0	 ;
            0	 ;
            K_1*K_pp	];

% Elevation dynamics
Ac_e = [ 0,         1;
        -K_3*K_ed,  -K_3*K_ep];
Bc_e = [ 0;
         K_3*K_ep];

% Complete system dynamics
Ac = blkdiag(Ac_lp, Ac_e);
Bc = blkdiag(Bc_lp, Bc_e);