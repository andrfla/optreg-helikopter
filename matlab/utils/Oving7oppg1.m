clear all; clc; close all;
%% 1d
N = 30;
A = [0 0 0; 0 0 1; 0.1 -0.79 1.78];
B = [1; 0; 0.1];
C = [0 0 1];
r = 1;
R = 2*r;
Q = 2*diag(C);
x0 = [0;0;1];

A_in = zeros(3*N,4*N);
B_in = zeros(3*N,1);
A_eq = [[zeros(3,3*N); kron(eye(N-1),-A), zeros(3*(N-1),3)] + eye(3*N), kron(eye(N),-B)];
B_eq = [A*x0; zeros(3*(N-1),1)];
G = [kron(eye(N),Q) zeros(3*N,N); zeros(N,3*N) R*eye(N)];
f = zeros(4*N,1);

uber_A = [G A_eq'; A_eq zeros(3*N)];
uber_b = [zeros(4*N,1); B_eq];
uber_x = linsolve(uber_A,uber_b);
%z = uber_x(1:4*N);

%% 1e
%[z,fval,exitflag,output] = quadprog(G,f,A_in,B_in,A_eq,B_eq);

%% 1f
A_u_lower = eye(N);
B_u_lower = ones(N,1);
A_u_upper = -eye(N);
B_u_upper = ones(N,1);
A_in = [zeros(N,3*N) A_u_lower; zeros(N,3*N) A_u_upper];
B_in = [B_u_lower; B_u_upper];

[z,fval,exitflag,output] = quadprog(G,f,A_in,B_in,A_eq,B_eq);

%% Calculate+plot
x = z(1:3*N);
u = z(3*N+1:4*N);
x1 = x(1:3:3*N);
x2 = x(2:3:3*N);
x3 = x(3:3:3*N);

hold on
%plot(x1,'b')
%plot(x2,'g')
plot(x3,'y')
plot(u,'k')
legend('y_t','u_t')
hold off
