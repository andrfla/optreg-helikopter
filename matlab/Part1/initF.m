%Denne fila inneholder initialisering for prosjektoppgave 2 i SIE3030. Den skal 
%brukes bare for helikopteret p� fors�kshallen. En annen init-fil er laget for
%helikoptrene p� Fische.

%%%%%%%%%%% Kalibrering av encoder og hw-oppsett for aktuelt helikopter
KalibVandring = -.0910;
KalibPitch = .0879;
KalibElevasjon = .0879;
EncoderInputVandring = 0;
EncoderInputPitch = 1;
EncoderInputElevasjon = 2;
joystick_gain_x = 1;
joystick_gain_y = 1;

%%%%%%%%%%% Faste fysiske verdier gitt initielt
m_w = 1.799; % motvekt
m_h = 1.34; % helikopter
delta_l_a = -0.07; % tuning av pivot-massesenter
l_a = 0.67 + delta_l_a; % lengde fra pivotpunkt til helikopterkropp
l_h = 0.177; % lengde fra midten p� helikopterkroppen til motor
J_e = 2 * m_h * l_a *l_a; % treghetsmoment om elevasjonsaksen
J_p = 2 * ( m_h/2 * l_h * l_h); % treghetsmoment om pitchaksen
J_t = 2 * m_h * l_a *l_a; %treghetsmoment om vandringsaksen
m_g=0.032; % Effektiv masse (med motvekt)
K_p = m_g*9.81; % Kraften som trengs for � holde helikopteret i likevekt.
V_f_eq=1.2; %Bytt verdi slik at det stemmer med det aktuelle helikopteret
V_b_eq=.6;%0.85;%1.2; %Bytt verdi slik at det stemmer med det aktuelle helikopteret
V_s_eq=V_f_eq+V_b_eq; % Minimum spenning for � holde helikopteret i likevekt
K_f = K_p/V_s_eq; % Kraftkonstant (N/V)

K_1 = l_h*K_f/J_p;
K_2 = K_p*l_a/J_t;
K_3 = K_f*l_a/J_e;
K_4 = K_p*l_a/J_e;

% Elevation controller parameters
K_ep = 7;5.5;
K_ed = 10;
K_ei = 2.1;

% Pitch controller parameters
w_c  = 5;
K_pd = w_c/K_1;
K_pp = (sqrt(2)*w_c^2)/K_1;
