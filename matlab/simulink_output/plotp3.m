close all

p3_2 = load('p3_2.mat');
subplot(311);
plot(p3_2.states(1,:),p3_2.states(2,:),'r',0:25/100:25,x1,'b')
title('$\mathrm{\lambda}  \mathrm{versus}  {\mathrm{\lambda^*}}$','interpreter','latex')
legend('\lambda','\lambda^*','interpreter','latex')

subplot(312)
plot(u_p32.states(1,:),u_p32.states(2,:)-u_p32.states(3,:))
title('$\Delta \mathrm{u}$','interpreter','latex')
legend('\Delta u' ,'interpreter','latex')

u_p32 = load('u_p32.mat');
pitch_p32 = load('pitch_p32.mat');
subplot(313);
plot(u_p32.states(1,:),u_p32.states(2,:),'r',u_p32.states(1,:),u_p32.states(3,:),'b',u_p32.states(1,:),(pi/180)*pitch_p32.states(2,1:size(u_p32.states,2)),'m')
title('u versus p varsus $\mathrm{u^*}$','interpreter','latex')
legend('u^*','u', 'p' ,'interpreter','latex')

