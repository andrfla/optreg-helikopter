clear; clc;
run ../../matlab/system_model;
addpath('../../matlab/utils/')
lambda_f = 0;
h = 0.25;
sec_before_start = 5;

% Discretize model
A = Ac_lp*h+eye(4)
B = Bc_lp*h
% [A B] = c2d(Ac_lp,Bc_lp,h)
%Problem size
n_x = size(A,2);
n_u = size(B,2);

% Initial values
lambda_0 = pi;
r_0 = 0;
p_0 = 0;
p_dot_0 = 0;
x0 = [lambda_0 r_0 p_0 p_dot_0]';
u0 = 0;

% Time horizon and initialization
N  = 60;			% Time horizon for states
M  = N;				% Time horizon for inputs
n_z = N*n_x+M*n_u;
z  = zeros(n_z,1);	% Initialize z for the whole horizon
z0 = z;           	% Initial value for optimization
z0(1) = lambda_0;

% Bounds
ul = -30*pi/180;		% Lower bound on control -- u1
uu = 30*pi/180;         % Upper bound on control -- u1
xl = -Inf*ones(n_x,1);	% Lower bound on states (no bound)
xu = Inf*ones(n_x,1);	% Upper bound on states (no bound)

% Generate the matrix Q and the vector c (objecitve function weights in the QP problem) 
q=1;
Q1 = zeros(n_x,n_x);
Q1(1,1) = 1;	% Weight on travel
P1 = q;			% Weight on pitch reference
Q = 2*genq2(Q1,P1,N,M,n_u);
c = [kron(ones(N,1),[-2*lambda_f;0;0;0]); zeros(N,1)];

% Generate system matrixes for linear model
Aeq = [[zeros(n_x,n_x*N); kron(eye(N-1),-A), zeros(n_x*(N-1),n_x)] + eye(n_x*N), kron(eye(N),-B)];
beq = [A*x0; zeros(n_x*(N-1),1)];

lb = [kron(ones(N,1),xl);kron(ones(N,1),ul)];
ub = [kron(ones(N,1),xu);kron(ones(N,1),uu)];
lb(N*n_x+M*n_u)  = 0; % We want the last input to be zero
ub(N*n_x+M*n_u)  = 0; % We want the last input to be zero

% Solve Qp problem with linear model
option = optimset('MaxIter',1000);
tic
[z,lambda] = quadprog(Q,c,zeros(n_z),zeros(n_z,1),Aeq,beq,lb,ub,z0,option);
optimTime = toc;

% Calculate objective value
phi1 = 0.0;
PhiOut = zeros(N*n_x+M*n_u,1);
for i=1:N*n_x+M*n_u
  phi1=phi1+Q(i,i)*z(i)*z(i);
  PhiOut(i) = phi1;
end

% Extract control inputs and states
u  = [z(N*n_x+1:N*n_x+M*n_u);z(N*n_x+M*n_u)]; % Control input from solution
x1 = [x0(1);z(1:n_x:N*n_x)];
x2 = [x0(2);z(2:n_x:N*n_x)];
x3 = [x0(3);z(3:n_x:N*n_x)];
x4 = [x0(4);z(4:n_x:N*n_x)];

num_zeros = sec_before_start/h;
Zeros = zeros(num_zeros,1);
Ones  = ones(num_zeros,1);
u = [Zeros; u; Zeros];
x1 = [pi*Ones; x1; Zeros];
x2 = [Zeros; x2; Zeros];
x3 = [Zeros; x3; Zeros];
x4 = [Zeros; x4; Zeros];

% figure
t = [0:h:h*(length(u)-1)]';
lambda = x1;
lambda_dot = x2;
pitch = x3;
pitch_dot = x4;

figure;
subplot(211);
stairs(t,u,'linewidth',2)
title('u','interpreter','latex')
box off;
subplot(212);
stairs(t,x3,'linewidth',2)
title('p','interpreter','latex')
 box off;
% save('quadprog_q1','t','u','lambda','lambda_dot','pitch','pitch_dot');
