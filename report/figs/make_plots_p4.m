QUARC_path = 'C:\ProgramData\QUARC\spool\windows\';

line_width = 2;
sim = load('mat_files/fmincon_p44.mat');
% simulink_data = load([QUARC_path 'states_p44_no_feedback.mat']);
% save('mat_files/states_p44_no_feedback.mat','simulink_data');
data = load('mat_files/states_p44_no_feedback.mat');
data = data.simulink_data;
t_ref = sim.t;
t = data.states(1,:);
e = 180./pi.*data.states(6,:);
e_ref = (180/pi)*sim.elev;
lambda = (180/pi)*data.states(2,:);
lambda_ref = 180./pi.*sim.lambda;
e_constraint = 180./pi.*alpha*exp(-beta*((sim.lambda-lambda_t)).^2);

subplot(211); hold on; % elevation
plot(t,e,['r' '--'],'linewidth',line_width)
stairs(sim.t, e_ref, 'r', 'linewidth', line_width);
stairs(sim.t,e_constraint,'linewidth',line_width)
xlim([5,20]);
legend('Measured elevation','Calculated elevation', 'Elevation constraint');

subplot(212); hold on; % travel
plot(t,lambda,['b' '--'],'linewidth',line_width);
stairs(t_ref,lambda_ref,'b','linewidth',line_width);
plot(t_ref,zeros(size(t_ref,1),1),'k--');
xlim([5,20]);
legend('Measured travel','Calculated travel');
hold off;