lambda_ref = 180;

close all; clear all; clc;
multiFigs =0; %Change to get several figures(1), or subfigures(0)
if multiFigs
%% Figure handles
uFig = figure; clf; hold all;
lambFig = figure; clf; hold all;
rFig = figure; clf; hold all;
pFig = figure; clf; hold all;
pdotFig = figure; clf; hold all;
%% q = 0.1
load('quadprogq01.mat');
figure(uFig);
plot(t,u,'k--');
figure(lambFig);
plot(t,x1,'k--');
figure(rFig);
plot(t,x2,'k--');
figure(pFig);
plot(t,x3,'k--');
figure(pdotFig);
plot(t,x4,'k--');
%% q = 1
load('quadprogq1.mat');
figure(uFig);
plot(t,u,'k:');
figure(lambFig);
plot(t,x1,'k:');
figure(rFig);
plot(t,x2,'k:');
figure(pFig);
plot(t,x3,'k:');
figure(pdotFig);
plot(t,x4,'k:');
%% q = 10
load('quadprogq10.mat');
figure(uFig);
plot(t,u,'k');
figure(lambFig);
plot(t,x1,'k');
figure(rFig);
plot(t,x2,'k');
figure(pFig);
plot(t,x3,'k');
figure(pdotFig);
plot(t,x4,'k');
% Set legends and titles
figure(uFig)
legend('q=0.1','q=1','q=10');
title('u')
figure(lambFig)
legend('q=0.1','q=1','q=10');
title('lambda')
figure(rFig)
legend('q=0.1','q=1','q=10');
title('r')
figure(pFig)
legend('q=0.1','q=1','q=10');
title('p')
figure(pdotFig)
legend('q=0.1','q=1','q=10');
title('pdot')

else
figure; clf; 
%% q = 0.1
load('quadprogq01.mat');
subplot(511); hold on;
plot(t,u,'k--');
subplot(512); hold on;
plot(t,x1,'k--');
subplot(513); hold on;
plot(t,x2,'k--');
subplot(514); hold on;
plot(t,x3,'k--');
subplot(515); hold on;
plot(t,x4,'k--');
%% q = 1
load('quadprogq1.mat');
subplot(511);
plot(t,u,'k:');
subplot(512);
plot(t,x1,'k:');
subplot(513);
plot(t,x2,'k:');
subplot(514);
plot(t,x3,'k:');
subplot(515);
plot(t,x4,'k:');
%% q = 10input
load('quadprogq10.mat');
subplot(511);
plot(t,u,'k');
title('$\mathrm{p}_{\mathrm{c}}$','interpreter','latex')
legend('q=0.1','q=1','q=10');
subplot(512);
plot(t,x1,'k');
title('$\lambda$','interpreter','latex')
legend('q=0.1','q=1','q=10');
subplot(513);
plot(t,x2,'k');
title('r')%,'interpreter','latex')
legend('q=0.1','q=1','q=10');
subplot(514);
plot(t,x3,'k');
title('p','interpreter','latex')
legend('q=0.1','q=1','q=10');
subplot(515);
plot(t,x4,'k');
title('$\dot{\mathrm{p}}$','interpreter','latex')
legend('q=0.1','q=1','q=10');
end