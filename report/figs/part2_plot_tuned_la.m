close all; clear all; clc;
%time, lambda, lambda_dot, 
normal_data = load('mat_files/ex2_q1.mat');
normal_quadprog = load('mat_files/quadprogq1.mat');
tuned_data = load('mat_files/part2_tuned_la_q1.mat');
tuned_quadprog = load('mat_files/part2_tuned_la_q1_quadprog.mat');

normal_travel = (180/pi)*normal_data.states(2,:)+180;
n_r = (180/pi)*normal_data.states(3,:);
normal_time = normal_data.states(1,:);

tuned_travel = (180/pi)*tuned_data.states(2,:)+180;
t_r = (180/pi)*tuned_data.states(3,:);
tuned_time = tuned_data.states(1,:);

%filter
a = 0.001/0.1;
t_r = filter(a, [1 a-1], t_r);
n_r = filter(a, [1 a-1], n_r);

calc_n_r = (180/pi)*normal_quadprog.lambda_dot;
calc_t_r = (180/pi)*tuned_quadprog.lambda_dot;
calc_n_t = normal_quadprog.t;
calc_t_t = tuned_quadprog.t;



refx = linspace(0,30); refy = zeros(100,1);

figure; hold on; %travel vs travel
s = plot(tuned_time,tuned_travel,'b',normal_time,normal_travel,'r');
s2 = plot(normal_quadprog.t,(180/pi)*normal_quadprog.lambda,'r--');
plot(refx,refy,'k--')

set(s,'linewidth',2);
set(s2,'linewidth',2);
l = legend('after adjustment, measured','before adjustment, measured',...
	'before adjustment, calculated');
set(l,'FontSize',12);
title('$\lambda$','interpreter','latex');
box off; xlim([5 20]);

figure; hold on;
p1 = plot(tuned_time,t_r,'b',calc_t_t,calc_t_r,'b--');
p2 = plot(normal_time,n_r,'r',calc_n_t,calc_n_r,'r--');
xlim([5 20]);
set(p1,'linewidth',2)
set(p2,'linewidth',2)
leg = legend('after tuning, measured','after adjustment, predicted',...
	'before tuning, measured','before adjustment, predicted',...
	'location','nw');
set(leg,'FontSize',12)%,'FontName','Comic Sans');
title('r');
