function plot_pitch_travel_p2(inputVars, outputVars,color,pos)
lw = 2;
ref = load(inputVars);
data = load(outputVars);
t_ref = ref.t;
t = data.states(1,:);
pitch = 180./pi.*data.states(4,:);
pitch_ref = ref.u.*180./pi;
lambda = 180./pi.*data.states(2,:);
lambda_ref = 180./pi.*ref.lambda-180;
figure(1); hold on; subplot(pos); hold on; %pitch 
plot(t,pitch,[color '--'],'linewidth',lw)
stairs(t_ref,pitch_ref,color,'linewidth',lw);
xlim([5,20]);
figure(2); hold on; subplot(pos); hold on; %lambda
plot(t,lambda,[color '--'],'linewidth',lw);
stairs(t_ref,lambda_ref,color,'linewidth',lw);
plot(t_ref,-180*ones(size(t_ref,1),1),'k--');
xlim([5,20]);	
