close all; clear all; clc;

p3_2 = load('mat_files/p3_2.mat');
x1 = load('mat_files/p3_x1.mat');
subplot(211);
hold on
stairs(0:25/100:25,x1.x1(:),'b');
plot(p3_2.states(1,:),p3_2.states(2,:),'r')
xlim([0 35])
hold off
title('$\lambda$ and $\lambda^*$','interpreter','latex')
l = legend('$\lambda$','$\lambda^*$');
set(l,'interpreter','latex','FontSize',12)

% subplot(312)
% plot(u_p32.states(1,:),u_p32.states(2,:)-u_p32.states(3,:))
% title('$\Delta \mathrm{u}$','interpreter','latex')
% legend('\Delta u' ,'interpreter','latex')

u_p32 = load('mat_files/u_p32.mat');
pitch_p32 = load('mat_files/pitch_p32.mat');

a = 0.001/0.1;
filtered = filter(a, [1 a-1], u_p32.states(3,:));

subplot(212);
plot(u_p32.states(1,:),u_p32.states(2,:),'r',...
	u_p32.states(1,:),filtered,'b',...
	u_p32.states(1,:),(pi/180)*pitch_p32.states(2,1:size(u_p32.states,2)),'g')
title('$\mathrm{u^*}$, u and p','interpreter','latex')
l2 = legend('u$^*$','u', 'p');
set(l2,'interpreter','latex','FontSize',12);

