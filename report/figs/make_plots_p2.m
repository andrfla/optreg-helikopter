clear all; close all; clc;
addpath('mat_files/')
plot_pitch_travel_p2('quadprogq01.mat','ex2_q01.mat','r',311)	
plot_pitch_travel_p2('quadprogq1.mat','ex2_q1.mat','b',312)
plot_pitch_travel_p2('quadprogq10.mat','ex2_q10.mat','g',313)
for fig = [1 2]
	figure(fig);
	for subFig = [311 312 313]
		subplot(subFig);
		if fig == 1 %pitch
			legend('Measured pitch','Calculated pitch reference');
		elseif fig == 2 %position
			legend('Measured position','Calculated position',...
				'Desired position');
		end
		if subFig == 311
			title('q=0.1')
		elseif subFig == 312
			title('q=1')
		elseif subFig == 313
			title('q=10')
		end
	end
end