\section{Optimal control of pitch/travel without feedback}
\label{sec:task2}
We will now use quadratic programming to calculate an optimal trajectory for the pitch and travel angles. The calculated sequence will be given as reference to the pitch controller, and the helicopter will try to follow that course without any feedback from the system. We will, in this part, assume the elevation is zero. This means we can disregard the elevation dynamics in our model.
\subsection{Continous state-space model}
We begin by writing the system derived in \cite{oppgave} shown in \eqref{eq:closed_loop_p_l} on state-space form, yielding the system equation in \eqref{eq:cont_time_travel}.
\begin{subequations}
	\label{eq:closed_loop_p_l}
\begin{equation}
	\ddot{p} = -K_1 K_{pd} \dot{p} - K_1 K_{pp} p + K_1 K_{pp} p_c 
\end{equation}
\begin{equation}
	\dot{\lambda} = r 
\end{equation}
\begin{equation}
	\dot{r} = -K_2 p
\end{equation}
\end{subequations}
\begin{align}
	\m{\dot{x}} &= \m{A_c}\m{x} + \m{B_c}u \notag \\
	\begin{bmatrix}
		\dot{\lambda} \\
		\dot{r} \\
		\dot{p} \\
		\ddot{p}
	\end{bmatrix} &= 
	\underbrace{
			\begin{bmatrix}
				0 & 1 & 0 & 0 \\
				0 & 0 & -K_2 & 0 \\
				0 & 0 & 0 & 1 \\
				0 & 0 & -K_1 K_{pp} & -K_1 K_{pd}
			\end{bmatrix}}_{\m{A_c}}
			\begin{bmatrix}
				\lambda \\
				r \\
				p \\
				\dot{p}
			\end{bmatrix}
		+
	\underbrace{
			\begin{bmatrix}
				0 \\
				0 \\
				0 \\
				1
			\end{bmatrix}}_{\m{B_c}} p_c \label{eq:cont_time_travel}
\end{align}
This models the closed loop pitch dynamics and the open loop travel dynamics. We do not include the generated voltages from the PD pitch controller to the actuators, or any of the elevevation dynamics. With regards to Figure 7 in the assignment \cite{oppgave}, we have modeled the basic control layer and the physical layer, with $p_c$ corresponding to the optimization layer output $u^*$. 
\subsection{Discretized model}
Using forward Euler to discretize our system with timestep $h$, we get the following equations:
\begin{subequations}
\label{eq:euler}
\begin{align}
	&\m{\dot{x}}(t=k\cdot h)\approx \frac{\m{x_{k+1} - x_k}}{h} = \m{A_c}\m{x_k} + \m{B_c}u_k \\
	&\Rightarrow \m{x_{k+1}} = \underbrace{ \left(\m{A_c}h + \m{I}\right)}_{\m{A}} \m{x_k} 
	+ \underbrace{ \m{B_c}h }_{\m{B}} \cdot u_k
\end{align}
\end{subequations}
We can also use the built-in function \verb!c2d! in Matlab. This is called exact discretization, and uses the matrix exponential. \cite{chen} give the equations for this discretization:
\begin{subequations}
\label{eq:exact}
\begin{align}
	\m{A} &= e^{\m{A_c} h} \\
	\m{B} &= \int_{\xi = 0}^{h} e^{\m{A_c} \xi} \mathrm{d}\xi \cdot \m{B_c}
\end{align}
\end{subequations}
We tested both methods on our systems in order to decide which method we should use.
They are compared in Figure~\ref{fig:euler_disc} and \ref{fig:exact_disc}, where we have plotted the calculated input reference $u=p_c$ and the estimated pitch $p$ for each method. 

The differences in estimated input are minor. However, the estimate for the pitch hints that the Euler discretization is unstable for the seleted value of $h$. This is confirmed by calculating the eigenvalues of the matrix $A$, which is found to be
\begin{align*}
&\lambda_1=\lambda_2=1 \\
&\lambda_{3,4}=0.375\pm1.379j
\end{align*}
The complex conjugate pair lies outside the unit circle, which means the system is unstable \cite{chen}.

\begin{figure}
\centering
\includegraphics[width=\textwidth]{figs/discretization_euler}
\caption{Input $u$ and predicted pitch $p$ with Euler discretization, five second pre and post zero padding}
\label{fig:euler_disc}
\end{figure}

\begin{figure}
\centering
\includegraphics[width=\textwidth]{figs/discretization_exact}
\caption{Input $u$ and predicted pitch $p$ with exact discretization, five second pre and post zero padding}
\label{fig:exact_disc}
\end{figure}

The only advantage we see in using Euler discretization is that the matrices $A$ and $B$ are slightly sparser. The matrices for exact discretization are shown in \eqref{eq:part2_c2d}, and the Euler discretization in \eqref{eq:part2_euler}. The increased density will increase the runtime of the \verb!quadprog!-function, but this is not an issue since all optimization is done offline. For this reason, we will continue with the matrices from the exact discretization part.
\begin{equation}
\m{A}=
\begin{bmatrix}
1.0 & 0.25 & -0.0053 & -0.0003 \\
0 & 0.999 & -0.0366 & -0.0035 \\ 
0 & 0 & 0.3598 & 0.0968 \\ 
0 & 0 & -3.4217 & -0.1241
\end{bmatrix}
\quad
\m{B}=
\begin{bmatrix}
-0.0008\\ 
-0.0122\\ 
0.6402\\ 
3.4217
\end{bmatrix}
\label{eq:part2_c2d}
\end{equation}
\begin{equation}
\m{A}=
\begin{bmatrix}
1.000 & 0.250 & 0 & 0 \\
0 & 1.000 & -0.0488 & 0\\
0 & 0 & 1.000 & 0.2500 \\
0 & 0 & -8.8388 & -0.2500
\end{bmatrix}
\quad
\m{B}=
\begin{bmatrix}
0 \\
0 \\
0 \\
8.8388
\end{bmatrix}
\label{eq:part2_euler}
\end{equation}

\subsection{Calculating an optimal trajectory and input}
The objective function is given as 
\begin{equation}
\phi=\sum_{i=1}^N\left(\lambda_i-\lambda_f\right)^2+qp_{ci}^2
\label{eq:part2_objective}
\end{equation}
A problem with this objective function is that it does not directly punish high travel angle- or pitch angle rates. As we can see in Figure~\ref{fig:p2_lambda}, this can lead to drift in the travel angle.

The $\left(\lambda_i - \lambda_f\right)^2$ term in the objective function \eqref{eq:part2_objective} will be larger for the starting iterations, and hence will be weighted more than the pitch reference $p_{ci}$. This can lead to a relatively large pitch reference at the initial iterations.
\subsection{Implementation of the input sequence}
Our implementation code is included in Code~\ref{lst:prob2}, and our simulink diagram is shown in Figure~\ref{fig:p2_model}. To test the optimization for different values of $q$ we first ran the optimization, then ran simulink with the calculated input sequence. We then compared the calculated input sequences to the measured angles. The pitch is shown in Figure~\ref{fig:p2_pitch}, and the traveled angle is shown in Figure~\ref{fig:p2_lambda}. The observed deviation is mainly due to modeling errors. The modeling errors include unmodeled dynamics like friction/air resistance, actuator dynamics and linearization. The linearization effects are more prominent for high pitch and/or elevation angles. Other sources of errors include calibration errors in pitch/elevation and measurement/actuator noise.

We observed that the most significant deviation was that the travel speed was much higher in the real system. This means that the controller puts too much energy into the system when trying to reach a specific speed. By studying our model, we noticed two effects that could cause this. Either the linearization of the $\sin p$ or the constant term in the travel rate dynamics. The linearization is an overestimate, so this should lead to a higher energy input. But what we are looking for is something that causes less energy in our model than in the real system. This lead us to look at the moment of inertia about the travel axis. We ended up moving the center of mass a bit back, used in the calculated moment of inertia. This gave some good results shown in Figure~\ref{fig:sabla_bra_lambda} and Figure~\ref{fig:sabla_bra_lambda_dot}. The code to tune the center of mass is shown in Code~\ref{lst:com_tuning}.

\begin{figure}
\includegraphics[width=\textwidth]{figs/tuning_travel.eps}
\caption{Open loop travel before and after adjusting the center of mass}
\label{fig:sabla_bra_lambda}
\end{figure}
\begin{figure}
\includegraphics[width=\textwidth]{figs/tuning_travel_rate.eps}
\caption{Open loop travel rate before and after adjusting the center of mass}
\label{fig:sabla_bra_lambda_dot}
\end{figure}

The deviations are further counteracted by adding feedback with an LQR controller in the next part of the assignment.
\begin{figure}
\includegraphics[width=\textwidth]{figs/prob2_pitch.eps}
\caption{Open loop pitch response for different $q$ values}
\label{fig:p2_pitch}
\end{figure}
\begin{figure}
\includegraphics[width=\textwidth]{figs/prob2_lambda.eps}
\caption{Open loop travel response for different $q$ values}
\label{fig:p2_lambda}
\end{figure}
