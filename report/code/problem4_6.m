% Generate bounds for QP	
ul = -30*pi/180*ones(n_u,1); 
uu = 30*pi/180*ones(n_u,1); 
lb = [kron(ones(N,1),xl);kron(ones(N,1),ul)];
ub = [kron(ones(N,1),xu);kron(ones(N,1),uu)];
lb(end-(n_u-1):end) = zeros(n_u,1);
ub(end-(n_u-1):end) = zeros(n_u,1);
%We want the last states to be small
delta_s = 3*pi/180;
lb((N-1)*n_x:N*n_x-1) = -delta_s*ones(n_x,1); 
ub((N-1)*n_x:N*n_x-1) = delta_s*ones(n_x,1);
