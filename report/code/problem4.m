lambda_f = 0; % final position
h = 0.25;
[A, B] = c2d(Ac, Bc, h); % Discrete model

% Initial values
lambda_0 = pi;
x0 = [lambda_0 0 0 0 0 0]';

% Time horizon and initialization
n_x = size(A,2);    % Number of states
n_u = size(B,2);    % Number of inputs
N  = 40;			% Time horizon for states
M  = N;				% Time horizon for inputs
n_z = N*n_x+M*n_u;
z  = zeros(n_z,1);	% Initialize z for the whole horizon
if exist('z_old')
    z0 = z_old;     % Initial value for optimization
else
    z0 = z;
end
z0(1) = lambda_0;

% Generate objective function parameters
q1 = 1;
q2 = 1;
Q1 = zeros(n_x,n_x);
Q1(1,1) = 1; % Weight on travel
P1 = zeros(n_u, n_u);
P1(1,1) = q1;
P1(2,2) = q2;
Q = 2*genq2(Q1,P1,N,M,n_u);
c = [kron(ones(N,1),[-2*lambda_f;0;0;0;0;0]); 
    zeros(M*n_u,1)];

% Calculate feedback gain
R_lqr = P1;
Q_lqr = diag([1 0.05 0.05 0.05 1 0.05]);
K_lqr = dlqr(A,B,Q_lqr,R_lqr);

% Generate system matrices for QP
Aeq = [[zeros(n_x,n_x*N); kron(eye(N-1),-A),...
    zeros(n_x*(N-1),n_x)] + eye(n_x*N), kron(eye(N),-B)];
beq = [A*x0; zeros(n_x*(N-1),1)];

% Generate bounds for QP
xl = -Inf*ones(n_x,1);	% No lower bound on states
xu = Inf*ones(n_x,1);	% No upper bound on states
ul = -30*pi/180*ones(n_u,1); % Lower bound on pitch/elev ref
uu = 30*pi/180*ones(n_u,1); % Upper bound on pitch/elev ref
lb = [kron(ones(N,1),xl);kron(ones(N,1),ul)];
ub = [kron(ones(N,1),xu);kron(ones(N,1),uu)];
lb(end-(n_u-1):end) = zeros(n_u,1); %Want last input to be zero
ub(end-(n_u-1):end) = zeros(n_u,1); %Want last input to be zero

% Make cost function and nonlinear constraint
alpha = 0.2;
beta = 20;
lambda_t = 2*pi/3; % 'obstacle' position
f = @(z) z'*Q*z + c'*z;
nonl_constr = @(z) (e_constraint(z, N, n_x, x0,...
    lambda_t, alpha, beta));

% Solve Nonlinear problem
z = fmincon(f, z0, [], [], Aeq, beq, lb, ub, nonl_constr);
z_old = z;

% Extract control input
u  = z(N*n_x+1:end);