function [ c_ineq, c_eq ] = e_constraint( z, N, n_x, x0,...
    lambda_t, alpha, beta )
    lambda = z(1:n_x:N*n_x);
    elevation = z(5:n_x:N*n_x);
    c_ineq = alpha*exp(-beta*(lambda-lambda_t).^2)-elevation;
    c_eq = 0;
end

