lambda_f = 0;
h = 0.25;
[A B] = c2d(Ac_lp,Bc_lp,h); % Discrete model

% Initial values
lambda_0 = pi;
x0 = [lambda_0 0 0 0]';
u0 = 0;

% Time horizon and initialization
n_x = size(A,2);    % Number of states
n_u = size(B,2);    % Number of inputs
N  = 60;			% Time horizon for states
M  = N;				% Time horizon for inputs
n_z = N*n_x+M*n_u;
z  = zeros(n_z,1);	% Initialize z for the whole horizon
z(1) = lambda_0;    % Initial z

% Generate objecitve function weights for the QP problem)
q = 1;
Q1 = zeros(n_x,n_x);
Q1(1,1) = 1;	% Weight on travel
P1 = q;			% Weight on pitch reference
Q = 2*genq2(Q1,P1,N,M,n_u);
c = [kron(ones(N,1),[-2*lambda_f;0;0;0]); zeros(N,1)];

% Generate system matrices and bounds for QP
Aeq = [[zeros(n_x,n_x*N); kron(eye(N-1),-A), ... 
    zeros(n_x*(N-1),n_x)] + eye(n_x*N), kron(eye(N),-B)];
beq = [A*x0; zeros(n_x*(N-1),1)];

%Generate bounds for QP
xl = -Inf*ones(n_x,1);	% No lower bound on states
xu = Inf*ones(n_x,1);	% No upper bound on states
lb = [kron(ones(N,1),xl);kron(ones(N,1),-30*pi/180)];   
ub = [kron(ones(N,1),xu);kron(ones(N,1),30*pi/180)];
lb(N*n_x+M*n_u) = 0; % Want last input to be zero
ub(N*n_x+M*n_u) = 0; % Want last input to be zero

% Solve QP
option = optimset('MaxIter',1000);
[z,lambda] = quadprog(Q,c,zeros(n_z),...
    zeros(n_z,1), Aeq,beq,lb,ub,z,option);

% Extract control input
u  = [z(N*n_x+1:N*n_x+M*n_u);z(N*n_x+M*n_u)];