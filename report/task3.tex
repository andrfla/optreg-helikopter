\section{Optimal control of pitch/travel with feedback}
We will now implement a feedback to keep the helicopter at the open loop optimal trajectory to compensate for the deviations previously discussed. The input $u_k$ for the pitch controller will now be calculated with 
\begin{equation}
	\label{eq:u_feedback}
	u_k = u_k^* - K^T(x_k-x_k^*)
\end{equation}
where $u_k^*$ is the optimal input, and $x^*$ is the optimal trajectory. $K$ is the feedback gain, which we will calculate with the LQR method. 
\subsection{Finding the optimal feedback gain}
Finding the optimal feedback gain is done by minimizing the cost function 
\begin{equation}
	J = \sum_{i=0}^{\infty} \Delta x_{i+1}^T Q \Delta x_{i+1} + B \Delta u_i, \quad Q \geq 0, R > 0
\end{equation}
with 
\begin{subequations}
\begin{gather}
	\Delta u = u - u^* \\
	\Delta x = x - x^*
\end{gather}
\end{subequations}

In matlab, the optimal gain $K$ can be calculated with the function \texttt{dlqr}. Using Bryson's rule \cite{bryson} is often a good choice designing a starting point for the weighting matrices Q and R. However, it requires an intuitive understanding of what reasonable maximum values will be. As our controller controls the difference between the optimal path and the actual path, it is hard to get an intuitive understanding of what's acceptable.
\begin{subequations}
	\begin{equation}
		q_{i,j} = \left\{
		\begin{array}{l l}
			\frac{1}{(\mathrm{max\ value\ of\ x_i})^2} \quad i = j \\
			0 \hspace{2.77cm} i \neq j
		\end{array} \right. 
	\end{equation}
\text{and}
	\begin{equation}
		r_{i,j} = \left\{
		\begin{array}{l l}
			\frac{1}{(\mathrm{max\ value\ of\ u_i})^2} \quad i = j \\
			0 \hspace{2.77cm} i \neq j
		\end{array} \right. 
	\end{equation}
\end{subequations}
To make it simple for ourselves, we will instead start with a simple $Q$ matrix, and tune it if we don't like the behavior. 
\begin{equation}
	Q = 
	\begin{bmatrix}
		1 & 0 & 0 & 0 \\
		0 & 1 & 0 & 0 \\
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1
	\end{bmatrix}
\end{equation}
With $R$ it is easier to get an idea of what reasonable maximum values for the output can be, and we choose
\begin{equation}
	R =\frac{1}{(\frac{\pi}{3})^2} = 0.9119
\end{equation}
We use the matlab script in Code~\ref{lst:prob3} to calculate the optimal feedback gain. This gives us 
\begin{equation}\label{eq:K_lqr}
	K = 
	\begin{bmatrix}
		-0.2245 & -1.0446 & -0.5474 & 0.0123 
	\end{bmatrix}
\end{equation}
which we will use as a starting point for tuning our LQR controller.

\subsection{Implementing the feedback controller}
To implement a feedback controller in Simulink we added a subsystem to resemble the LQR block from Figure 8 in \cite{oppgave}. The block calculates u from the optimal u$^*$ and x$^*$ returned from quadprog, as well as the actual state x from the helicopter. This is done by combining $K$ from \eqref{eq:K_lqr} with equation \eqref{eq:u_feedback}, as seen in Figure~\ref{fig:p3_model_sub}. The complete Simulink model can be seen in Figure~\ref{fig:p3_model}.

After some experimenting we ended up with the following weight matrix
\begin{equation}
	Q = 
	\begin{bmatrix}
		10 & 0 & 0 & 0 \\
		0 & 0.5 & 0 & 0 \\
		0 & 0 & 1 & 0 \\
		0 & 0 & 0 & 1
	\end{bmatrix}
\end{equation}
This gives 
\begin{equation}
	K = \begin{bmatrix} -0.6764 & -1.9357 & -0.4626 & 0.0212\end{bmatrix}
\end{equation}
This is a controller we think work well. Figure~\ref{fig:p32_plots} shows the travel compared to the simulated travel. In addition it shows the measured pitch $p$ compared to the optimal pitch reference $u^*$ and the actual pitch reference $u$.
\begin{figure}
\begin{center}
\includegraphics[width=\textwidth]{figs/p32}
\caption{Controller with feedback}
\label{fig:p32_plots}
\end{center}
\end{figure}

\subsection{Model Predictive Control}
To realize MPC instead of using the LQR strategy, we would have to calculate the optimal input sequence $u^*$ at each time step. We would then input the first element in the output sequence to the plant. This would increase the computational cost at each time step a lot. In practice we could not implement an MPC with the current time horizon length at the same update frequency on the same hardware.

We timed the calculation of the optimal trajectory to be about 3.5 seconds, while the fastest time constant in the system is about 0.4 seconds. This means that there can be significant changes in the states before we calculate a new optimal trajectory. We would need to either shorten our prediction horizon or get some faster hardware to use MPC directly on this plant.

The advantage of using MPC directly is that we can take the measured disturbances into consideration along our optimal trajectory. What the LQR does is to continously adjust our path along what we thought was optimal. This will, at least intuitively, create a less optimal path.
